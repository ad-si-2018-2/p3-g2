var app = require('express')();
var http = require('http').Server(app);
var io = require('socket.io')(http);

//Enviando a página
app.get('/', function(req, res){
  res.sendFile(__dirname + "/index.html");
});

//Evento de captura de conexão
io.on('connection', function(socket){
    console.log('Usuário Conectado');
    io.emit('login', '####Novo usuário entrou');
    socket.on('disconnect',function(){
      io.emit('logof', '####Usuário saiu');
      console.log('Usuário Disconectado')
    })
});


//Envento de captura de mensagem
io.on('connection', function(socket){
  socket.on('chat message', function(msg){
    console.log('message: ' + msg);
  });
});

//Envento de envio de mensagem
io.on('connection', function (socket){
  socket.on('chat message', function(msg){
    io.emit('chat message', msg);
  })
})


http.listen(3000, function(){
  console.log('listening on *:3000');
});
